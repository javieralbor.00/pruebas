import React, { Component } from 'react'
import logo from '../logo.svg';
import '../App.css';
import Input from "./Inputs";

class practica extends Component {
  render() {
    return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.css</code> and save to reload.
            </p>
            <Input label="Ingresa tu nombre completo" name="nombre" onChangeData={this.props.onChangeData} value={this.props.data.email} type="text"/> 
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            > 
             Hola mundo esta es mi primera practica de react js 
            </a>
            <button onClick={this.props.onCloseSession}>
                cerrar session
            </button>
          </header>
        </div>
    )
  }
}
export default  practica ;