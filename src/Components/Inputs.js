import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';

class Inputs extends Component {
    render() {
        return (
            <div>
                <TextField
                    required
                    id="standard-required"
                    label={this.props.label}
                    defaultValue="Email"
                    name={this.props.name} 
                    onChange={this.props.onChangeData} 
                    value={this.props.value} 
                    type={this.props.type}
                    margin="normal"
                  />
            </div>
        )
    }
}
export default Inputs ;