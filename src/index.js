import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import './index.css';


const renderApp = () => {
    const App = require('./App.js').default;
ReactDom.render(<BrowserRouter><App /></BrowserRouter>,document.getElementById('root'));
}

renderApp();

if (module.hot) {
	module.hot.accept('./App', () => {
		const NextRootContainer = require('./App.js').default;
		ReactDom.render(<BrowserRouter><NextRootContainer/></BrowserRouter>,document.getElementById('root'));
	});
}