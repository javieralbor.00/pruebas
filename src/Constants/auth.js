class Auth {
	isAuthenticated = false;
	async login(cb) {
	  await this.authenticated(true);
		setTimeout(cb, 100);
	}

	signout(cb) {
		this.isAuthenticated = false;
		setTimeout(cb, 100);
	}

	authenticated(bol) {
  this.isAuthenticated = bol;
	}
}
export default new Auth();
