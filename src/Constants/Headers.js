export const COLUMNS = [{
    title: 'Name', dataIndex: 'name', key:'name', width: 100,
  }, {
    title: 'Age', dataIndex: 'age', key:'age', width: 100,
  }, {
    title: 'Address', dataIndex: 'address', key:'address', width: 200,
  }, {
    title: 'Operations', dataIndex: '', key:'operations',
  }];

export const  DATOS = [
    { name: 'Jack', age: 28, address: 'some where', key:'1' },
    { name: 'Rose', age: 36, address: 'some where', key:'2' },
    { name: 'Rose', age: 36, address: ["trabajo","casa","escuela","casa abuela", "escuela ingles", "escuela"] , key:'3',},
  ];